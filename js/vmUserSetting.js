var vmUserSetting = function(settingType, controlType, name, prettyName, value) {
	// Used to carry the various properties of a user setting
    var self = this;
 
    self.settingType = ko.observable(settingType);
    self.controlType = ko.observable(controlType);
    self.name = ko.observable(name);
    self.prettyName = ko.observable(prettyName);
    self.value = ko.observable(value);

}