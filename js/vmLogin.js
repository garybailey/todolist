var vmLogin = function() {

    var self = this;

 	// Controls which cards are visible
    self.doingLogin = ko.observable(false);
    self.doingNewAccount = ko.observable(false);
    self.doingResetPassword = ko.observable(false);

    // Response message for displaying login errors or registration updates
    self.loginMsg = ko.observable();
 
    // Tracking for logged in status
    self.loginToken = ko.observable();
    self.loggedIn = ko.observable(false);

 	self.doLogin = function() {
 		console.log("DoLogin");
 		console.log(ViewModels.vmUser.email())
 		self.loginMsg("Login Done");
 		return true;
 	}

 	self.doCreateAccount = function() {
 		console.log("doCreateAccount");
 		self.loginMsg("Account Created");
 		return true;
 	}

 	self.doResetPassword = function() {
 		console.log("doResetPassword");
 		self.loginMsg("Reset Password Done");
 		return true;
 	}

}