var vmTodoListItem = function(item) {

    var self = this;

    self.ID = ko.observable(1);
    self.reportedIssue = ko.observable("This is a problem");
    self.priority = ko.observable(3);
    self.status = ko.observable("Open");

    self.notes = ko.observableArray([{"text":"This is a note"}]);
    self.comments = ko.observableArray([{"text":"This is a comment"}]);

    self.linkType = ko.observable("confluence");
    self.linkID = ko.observable(53);

}