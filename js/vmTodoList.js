var vmTodoList = function() {

    var self = this;

    self.todoList = ko.observableArray();

    self.todoListFiltered = function() {
        return ko.utils.arrayFilter(self.todoList(), function(item) {
            console.log(item);
            return (true);
        })
    }

/*
    var item = new vmTodoListItem();
    self.todoList.push(item);
*/

    self.loadConfluence = function() {
        // This uses ajax to load whatever confluence todos are associated with this user
        var uriSetting = ViewModels.vmUser.singleSetting("URLConfluence");
        var method = "GET"
        var request = {
            url: (uriSetting[0].value() + "/rest/mywork/latest/task"),
            dataType: "jsonp",
            jsonpCallback: "logResults"
        };
        $.ajax(request);
    }

    function logResults(json){
      console.log(json);
    }

}