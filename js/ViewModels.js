
var ViewModels = {
    vmCommon : new vmCommon(),
    vmLogin : new vmLogin(),
    vmUser : new vmUser(),
    vmTodoList : new vmTodoList()
}

ko.applyBindings(ViewModels);