var vmUser = function() {

    var self = this;

    // Basic user settings
    self.email = ko.observable("xyz@example.com");
    self.password = ko.observable(1234);
 
    self.firstName = ko.observable("Gary");
    self.lastName = ko.observable("Bailey");
    self.fullName = ko.computed(function() {
    	return (self.firstName() + " " + self.lastName());
    })
    self.mobile = ko.observable(555123456);
    self.mobileProvider = ko.observable("Verizon");

    self.settings = ko.observableArray();

    //Notification settings
    var data = new vmUserSetting("notification","checkbox","notifyEmail","Enable Emailing","true");
    self.settings.push(data);
    var data = new vmUserSetting("notification","checkbox","notifyText","Enable Texting","true");
    self.settings.push(data);
    var data = new vmUserSetting("notification","checkbox","confirmedEmail","Email Confirmed","true");
    self.settings.push(data);
    var data = new vmUserSetting("notification","checkbox","confirmedText","Texting Confirmed","true");
    self.settings.push(data);

    //Connection settings for Confluence instance
    var data = new vmUserSetting("integration","text","URLConfluence","Confluence URL","https://dumacbusinesssystems.atlassian.net/wiki");
    self.settings.push(data);

    self.settingsFiltered = function(type) {
        return ko.utils.arrayFilter(self.settings(), function(item) {
            console.log(item);
            return (item.settingType().toLowerCase().indexOf(type.toLowerCase()) > -1);
        })
    }

    self.singleSetting = function(name) {
        return ko.utils.arrayFilter(self.settings(), function(item) {
            return (item.name().toLowerCase().indexOf(name.toLowerCase()) > -1);
        })
    }

}