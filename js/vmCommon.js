var vmCommon = function() {

    var self = this;
 
    //Visibility controls
    self.visibleLogin = ko.observable(false);
    self.visibleUserSettings = ko.observable(false);
    self.visibleTodoList = ko.observable(true);

    //Logged in flag
    self.loggedIn = ko.observable(false);

    //List of mobile providers for using email to text notifications
    self.mobileProviderArray = ko.observableArray(["Verizon","T-mobile"]);

}