var vmItemStatus = function(statusLevel) {

    var self = this;

    var levelList = new Array(["Open","Closed","Monitoring"]);

    self.name = ko.observable(levelList[statusLevel]);
    self.sortOrder = ko.observable(statusLevel);

}